﻿using System;

namespace Domain.Time
{
    public interface ITimeProvider
    {
        DateTime Now { get; }
    }
}