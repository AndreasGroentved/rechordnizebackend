﻿using System;

namespace Domain.Time
{
    public class DateTimeWrapper : ITimeProvider
    {
        public DateTime Now => DateTime.Now;
    }
}