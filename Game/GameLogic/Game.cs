﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using Timer = System.Timers.Timer;

namespace Domain.GameLogic
{
    public class Game
    {
        private readonly Random _random = new Random();
        private readonly SemaphoreSlim _gameStateSemaphore;
        public  Subject<bool> StartGameSubject { get; } = new Subject<bool>();
        public  Subject<bool> StartOrStopTimeSubject { get; } = new Subject<bool>();
        private Timer _timer;
        public GameInfo GameInfo { get; }
        public string Id { get; }

        public Game(GameInfo gameState, string id)
        {
            GameInfo = gameState;
            _gameStateSemaphore = new SemaphoreSlim(1, 1);
            Id = id;
        }


        public void StartStartGameTimer(int timeInSeconds = 5)
        {
            if (_timer != null) return;
            _timer = new Timer(timeInSeconds * 1000);
            _timer.Elapsed += (sender, e) => InformGameStartTimer();
            _timer.Start();
            StartOrStopTimeSubject.OnNext(true);
        }

        public bool IsStarting()
            => _timer != null;

        public bool HasStarted()
            => GameInfo.State == GameState.Started;

        private void InformGameStartTimer()
        {
            NullTimer();
            StartGameSubject.OnNext(true);
        }

        private void NullTimer()
        {
            _timer?.Stop();
            _timer?.Dispose();
            _timer = null;
        }

        public void CancelStartGameTimer()
        {
            NullTimer();
            StartOrStopTimeSubject.OnNext(false);
        }

        public void SetStateOfPlayer(string playerId, State state)
            => GameInfo.SetStateOfPlayer(playerId, state);

        public bool RemovePlayer(string playerId) => RemovePlayerIfExist(playerId);

        private bool RemovePlayerIfExist(string playerId)
        {
            var wasPlayerRemoved = GameInfo.PlayerMap.TryRemove(playerId, out _);
            if (!wasPlayerRemoved) return false;
            if (GameInfo.HostPlayer == playerId)
                TryUpdatingHostAfterHostRemoval();
            return true;
        }

        private bool TryUpdatingHostAfterHostRemoval()
        {
            lock (GameInfo)
            {
                GameInfo.HostPlayer = "";
                var otherPlayers = GameInfo.PlayerMap.Count > 0;
                if (!otherPlayers) return false;
                GameInfo.HostPlayer = GameInfo.PlayerMap.First().Key;
                return true;
            }
        }

        public bool JoinGame(string userId)
        {
            var addedPlayer = AddPlayerIfAbsent(userId);
            return addedPlayer;
        }

        private bool AddPlayerIfAbsent(string userId)
        {
            GameInfo.PlayerMap.TryGetValue(userId, out var player);
            var alreadyAddedPlayer = player != null;
            if (!alreadyAddedPlayer)
                AddUserIdToGame(userId);

            return !alreadyAddedPlayer;
        }

        private void AddUserIdToGame(string userId)
        {
            GameInfo.PlayerMap[userId] = new GamePlayer(userId);
            lock (GameInfo)
            {
                if (string.IsNullOrEmpty(GameInfo.HostPlayer)) GameInfo.HostPlayer = userId;
            }
        }

        public void Start(DateTime starTime)
        {
            GameInfo.State = GameState.Started;
            StartGameSubject.Dispose();
            GameInfo.StartTime = starTime;
        }


        /// <summary>
        /// Checks if guess is correct and add guess to game if is correct and returns correctness
        /// </summary>
        /// <exception cref="System.InvalidOperationException">If playerId is invalid.</exception>
        /// <returns></returns>
        public bool Guess(GuessInfo guess)
        {
            ValidateThatPlayerExists(guess.PlayerId);
            var isCorrect = IsGuessCorrect(guess);
            if (isCorrect)
                IncrementScore(GetPlayer(guess.PlayerId));
            return isCorrect;
        }

        private void ValidateThatPlayerExists(string playerId)
        {
            if (GameInfo.PlayerMap.TryGetValue(playerId, out _) == false)
                throw new InvalidOperationException($"PlayerId {playerId} is invalid");
        }

        private GamePlayer GetPlayer(string playerName)
        {
            GameInfo.PlayerMap.TryGetValue(playerName, out var player);
            return player ?? throw new InvalidOperationException($"PlayerId: {playerName} is invalid");
        }

        private void IncrementScore(GamePlayer player)
        {
            player.Score += 1;
        }

        private bool IsGuessCorrect(GuessInfo guess)
        {
            if (GameUtil.GetType(guess.GuessList.First()) == PlayableExtra.ChordProgression) //TODO temp solution 
                guess.GuessList.Insert(0, "I maj");

            var correctSequence = GameInfo.Rounds[guess.RoundNumber - 1].PlayableNames;
            var correct = correctSequence.SequenceEqual(guess.GuessList);
            return correct;
        }

        public async Task<List<string>> GetNext(int roundNumber)
        {
            ThrowIfInvalidState(roundNumber);
            if (GameInfo.Rounds.Count < roundNumber) await GenerateNewRound(roundNumber);
            return GameInfo.Rounds[roundNumber - 1].PlayableNames;
        }

        private void ThrowIfInvalidState(int roundNumber)
        {
            if (!HasStarted()) throw new InvalidOperationException("Level has not yet started");
            if (roundNumber > GameInfo.Rounds.Count + 1) throw new InvalidOperationException("Invalid round number");
            if (roundNumber <= 0)
                throw new ArgumentOutOfRangeException($"{nameof(roundNumber)} must be greater than zero");
            if (HasGameEnded()) throw new InvalidOperationException("Game has ended");
        }

        private bool HasGameEnded() => GameInfo.State == GameState.Ended;

        public Dictionary<string, int> End()
        {
            GameInfo.State = GameState.Ended;
            return GameInfo.PlayerMap.ToDictionary(p => p.Key, p => p.Value.Score);
        }

        private async Task GenerateNewRound(int roundNumber)
        {
            await _gameStateSemaphore.WaitAsync();

            if (!HasRoundAlreadyBeenGenerated(roundNumber))
                AddRoundWithNumberToGame(roundNumber);

            _gameStateSemaphore.Release();
        }

        private void AddRoundWithNumberToGame(int roundNumber)
            => GameInfo.Rounds.Add(new Round(roundNumber, GenerateRoundPlayables()));

        private bool HasRoundAlreadyBeenGenerated(int roundNumber)
            => GameInfo.Rounds.Count == roundNumber + 1;

        private List<string> GenerateRoundPlayables()
            => new List<string> {GameInfo.Playables[_random.Next(GameInfo.Playables.Count)]};
    }
}