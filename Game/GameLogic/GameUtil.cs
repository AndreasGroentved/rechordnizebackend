﻿using System;
using System.Linq;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using ReChordNize.Backend.Model.Util;

namespace Domain.GameLogic
{
    public static class GameUtil
    {
        public static PlayableExtra GetType(string name)
        {
            if (name == null) return PlayableExtra.Invalid;
            var firstPart = name.Split(" ").First();
            if (ChordUtil.TypeNames.Contains(name)) return PlayableExtra.ChordType;
            if (ChordUtil.ProgressionNames.Contains(firstPart)) return PlayableExtra.ChordProgression;
            if (ChordUtil.NoteNames.Contains(firstPart)) return PlayableExtra.Chord;
            if (ChordUtil.IntervalNames.Contains(name)) return PlayableExtra.Interval;
            return PlayableExtra.Invalid;
        }
    }
}