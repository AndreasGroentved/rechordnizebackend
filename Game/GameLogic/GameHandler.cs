﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Timers;
using Domain.Time;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Model.Communication;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;

namespace Domain.GameLogic
{
    public class GameHandler
    {
        private readonly ConcurrentDictionary<string, Game> _runningGamesDictionary =
            new ConcurrentDictionary<string, Game>();

        private readonly ConcurrentDictionary<string, Game> _nonRunningGamesDictionary =
            new ConcurrentDictionary<string, Game>();

        private readonly ILevelRepo _levelRepo;
        private readonly long _defaultGameTimeInMin = 1;
        private readonly ITimeProvider _time;
        public Subject<List<UnStartedGame>> NonRunningGamesSubject { get; } = new Subject<List<UnStartedGame>>();
        public Subject<Response> GameEndedSubject { get; } = new Subject<Response>();
        public Subject<Response> StartGameObserver { get; } = new Subject<Response>();
        public Subject<Response> StartStopGameTimer { get; } = new Subject<Response>();
        public const long DefaultWaitTime = 5 /*s*/ * 1_000 /*ms*/;
        private const float DefaultStartGameLimit = 0.5f;


        public GameHandler(ILevelRepo levelRepo, ITimeProvider time)
        {
            _levelRepo = levelRepo;
            _time = time;
        }


        public async Task<string> GetGameOfType(LevelIdentifierEnum identifier) /*TODO spil kan starte i mellemtiden*/
            => _nonRunningGamesDictionary.Where(a =>
                    a.Value.GameInfo.IdentifierEnum == identifier && a.Value.GameInfo.PlayerMap.Count < 5)
                .OrderBy(a => a.Value.GameInfo.PlayerMap.Count).Select(a => a.Key).FirstOrDefault();

        public List<Game> NonRunningGames()
            => _nonRunningGamesDictionary.Values.ToList();


        public List<Game> RunningGames() =>
            _runningGamesDictionary.Values.ToList(); //TODO send mindre objekt

        public bool IsRunningGame(string gameId)
            => _runningGamesDictionary.ContainsKey(gameId);

        public async Task<Response> CreateNonRunningGame(LevelIdentifierEnum identifier, string gameId, string playerId)
        {
            var level = await _levelRepo.GetLevelWithIdentifier(identifier);
            if (level == null) return new ErrorResponse($"LevelIdentifier: {identifier} is invalid - CreateGame");
            var game = CreateGameObjectAndAddToNonRunningGames(identifier, gameId, playerId, level);
            SetGameListeners(gameId, game);
            return new GameCreatedResponse(game.Id);
        }

        private void SetGameListeners(string gameId, Game game)
        {
            game.StartGameSubject.Subscribe(async status => await HandleStartGame(gameId, status));
            game.StartOrStopTimeSubject.Subscribe(async status => await HandleGameStartOrCancel(gameId, status));
        }

        private Game CreateGameObjectAndAddToNonRunningGames(LevelIdentifierEnum identifier, string gameId,
            string playerId, Level level)
        {
            var playables = level.Playables;
            var gameState = new GameInfo(playables, playerId, _time.Now, identifier);
            var game = new Game(gameState, gameId);
            _nonRunningGamesDictionary.TryAdd(game.Id, game);
            NotifyUnStartedGamesChanged();
            return game;
        }


        private async Task HandleStartGame(string gameId, bool shouldStart)
        {
            if (shouldStart)
            {
                var res = await StartGame(gameId);
                StartGameObserver.OnNext(res);
            }
        }

        public async Task<Response> JoinGame(string gameId, string userId)
        {
            var foundGame = _nonRunningGamesDictionary.TryGetValue(gameId, out var game);
            if (!foundGame) return new ErrorResponse($"Game id: {gameId} is invalid - JoinGame");
            game.JoinGame(userId);
            return new GameJoinedResponse(gameId, game.GameInfo.PlayerMap.Keys.ToList(), userId,
                game.GameInfo.Playables);
        }

        private void NotifyUnStartedGamesChanged()
        {
            NonRunningGamesSubject.OnNext(NonRunningGames()
                .Select(g => new UnStartedGame(g.GameInfo.IdentifierEnum, g.Id)).ToList());
        }

        public async Task<Response> StartGame(string gameId)
        {
            var game = GetGameAndUpdateStartedUnStartedDictionaries(gameId);
            if (game == null) return new ErrorResponse($"GameId: {gameId} is invalid - StartGame");

            game.Start(_time.Now);
            SetTimer(_defaultGameTimeInMin, gameId);
            return new GameStartedResponse {GameId = gameId};
        }


        private Game GetGameAndUpdateStartedUnStartedDictionaries(string gameId)
        {
            var removedGame = _nonRunningGamesDictionary.TryRemove(gameId, out var game);
            if (!removedGame) return null; /*new ErrorResponse($"GameId: {gameId} is invalid - StartGame");*/
            _runningGamesDictionary.TryAdd(gameId, game);
            NotifyUnStartedGamesChanged();
            return game;
        }

        public async Task<Response> Guess(GuessInfo guessInfo, string gameId)
        {
            var foundGame = _runningGamesDictionary.TryGetValue(gameId, out var game);
            if (!foundGame) return new ErrorResponse($"GameId: {gameId} is invalid - Guess");
            try
            {
                var correct = game.Guess(guessInfo);
                return new GuessResponse(guessInfo.PlayerId, correct);
            }
            catch (InvalidOperationException e)
            {
                return new ErrorResponse(e.Message);
            }
        }

        public async Task<Response> EndGame(string gameId)
        {
            var removedGame = _runningGamesDictionary.TryRemove(gameId, out var game);
            if (!removedGame) throw new ArgumentException($"Invalid gameId {gameId} - EndGame");
            return new GameEndResponse(game.End(), game.Id);
        }

        public async Task<Response> Next(int roundNumber, string gameId)
        {
            var foundGame = _runningGamesDictionary.TryGetValue(gameId, out var game);
            if (!foundGame) return new ErrorResponse($"GameId: {gameId} is invalid - Next");

            return await GetNextRoundFromGameResponse(roundNumber, game);
        }

        private static async Task<Response> GetNextRoundFromGameResponse(int roundNumber, Game game)
        {
            try
            {
                var next = await game.GetNext(roundNumber);
                if (next == null) return new ErrorResponse($"Round number {roundNumber} is invalid");
                return new NextResponse(roundNumber, next);
            }
            catch (InvalidOperationException e)
            {
                return new ErrorResponse(e.Message);
            }
        }

        public async Task UnReadyPlayerFromGame(string playerId, string gameId)
        {
            var game = await GetGameRunningOrNonRunning(gameId);
            if (game == null) return;
            SetStateOfPlayerFromGameAndReturnGame(playerId, game, State.NotReady);
            await HandleStart(gameId, game);
        }

        public async Task ReadyPlayerFromGame(string playerId, string gameId)
        {
            var game = await GetGameRunningOrNonRunning(gameId);
            if (game == null) return;
            SetStateOfPlayerFromGameAndReturnGame(playerId, game, State.Ready);
            await HandleStart(gameId, game);
        }

        private async Task HandleStart(string gameId, Game game)
        {
            var shouldStart = await ShouldStartGame(gameId);
            if (shouldStart)
                game.StartStartGameTimer();
            else
                game.CancelStartGameTimer();
        }


        private const bool Delete = false;

        private async Task HandleGameStartOrCancel(string gameId, bool status) //kunne nok være enum
        {
            if (status == Delete)
                StartStopGameTimer.OnNext(new GameCancelStartResponse {GameId = gameId});
            else
                StartStopGameTimer.OnNext(new GameIsStartingResponse {GameId = gameId});
        }

        private void SetStateOfPlayerFromGameAndReturnGame(string playerId, Game game, State state)
        {
            game?.SetStateOfPlayer(playerId, state);
        }

        /// <summary>
        /// Returns either a running or nonrunning game, if game is not found, then null is returned
        /// </summary>
        public async Task<Game> GetGameRunningOrNonRunning(string gameId)
        {
            _runningGamesDictionary.TryGetValue(gameId, out var game);
            if (game != null) return game;
            _nonRunningGamesDictionary.TryGetValue(gameId, out game);
            return game;
        }


        public async Task<bool> ShouldStartGame(string gameId)
        {
            var game = await GetGameRunningOrNonRunning(gameId);
            return AreBeginConditionsValid(game); //TODO
        }

        private static bool AreBeginConditionsValid(Game game)
        {
            if (game == null) return false;
            var countOfPlayer = game.GameInfo.PlayerMap.Count;
            var readyPlayers = game.GameInfo.PlayerMap.Values.Count(a => a.State == State.Ready);
            return !game.IsStarting() && (readyPlayers / (float) countOfPlayer) > DefaultStartGameLimit;
        }

        public async Task<List<GamePlayer>> GetPlayersOfGame(string gameId)
        {
            var game = await GetGameRunningOrNonRunning(gameId);
            return game == null ? new List<GamePlayer>() : game.GameInfo.PlayerMap.Values.ToList();
        }


        public async Task<Response> LeaveGame(string gameId, string userId)
        {
            var game = await GetGameRunningOrNonRunning(gameId);
            if (game == null) return new ErrorResponse($"No game with id: {gameId} - LeaveGame");

            game.RemovePlayer(userId);
            return new GamePlayerListResponse {PlayerNameList = game.GameInfo.PlayerMap.Values.ToList()};
        }

        private void SetTimer(long timeInMinutes, string gameId)
        {
            var timer = new Timer(timeInMinutes * 60 * 1000);
            timer.Elapsed += async (sender, e) => await HandleTimer(gameId, timer);
            timer.Start();
        }

        private async Task HandleTimer(string gameId, Timer timer)
        {
            timer.Stop();
            timer.Dispose();
            var res = await EndGame(gameId);
            GameEndedSubject.OnNext(res);
        }
    }
}