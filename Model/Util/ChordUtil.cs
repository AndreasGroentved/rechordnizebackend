﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Util
{
    public static class ChordUtil
    {
        public static readonly IEnumerable<string> ProgressionNames = new List<string>
            {"I", "bII", "II", "bIII", "III", "IV", "bV", "V", "bVI", "VI", "bVII", "VII"};

        public static readonly IEnumerable<string> NoteNames = new List<string>
            {"Ab", "A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G"};

        public static readonly IEnumerable<string> TypeNames= new List<string>
        {
            "maj", "min", "dim", "b5", "min#5", "aug", "sus2", "sus4", "sus2sus4", "5", "6", "6/9", "min6", "7b5",
            "min7b5", "min7", "7", "maj7", "aug7", "minmaj7", "dim7", "9", "min9", "11", "min11", "13", "min13", "maj13"
        };


        public static readonly IEnumerable<string> IntervalNames = new List<string>
        {
            "P1", "m2", "M2", "m3", "M3", "P4", "A4", "P5",
            "m6", "M6", "m7", "M7", "P8", "m9", "M9", "m10",
            "M10", "P11", "A11", "P12", "m13", "M13", "m14", "M14",
            "P15"
        };
    }
}