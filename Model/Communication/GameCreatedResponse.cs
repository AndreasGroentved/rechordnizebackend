﻿namespace ReChordNize.Backend.Model.Communication
{
    public class GameCreatedResponse : Response
    {
        public GameCreatedResponse(string gameId)
        {
            GameId = gameId;
        }

        public string GameId { get; set; }
    }
}