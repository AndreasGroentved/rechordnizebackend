﻿namespace ReChordNize.Backend.Model.Communication
{
    public class NewGameResponse : Response
    {
        public NewGameResponse(string gameId, bool didJoinSuccessfully)
        {
            GameId = gameId;
            DidJoinSuccessfully = didJoinSuccessfully;
        }

        public string GameId { get; set; }
        public bool DidJoinSuccessfully { get; set; }
    }
}