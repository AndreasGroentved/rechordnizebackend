﻿namespace ReChordNize.Backend.Model.Communication
{
    public class GameIsStartingResponse : Response
    {
        public string GameId { get; set; } = "Error";
    }
}