﻿namespace ReChordNize.Backend.Model.Communication
{
    public class GameStartedResponse : Response
    {
        public string GameId { get; set; } = "Error";
    }
}