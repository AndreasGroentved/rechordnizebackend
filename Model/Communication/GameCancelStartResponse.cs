﻿namespace ReChordNize.Backend.Model.Communication
{
    public class GameCancelStartResponse : Response
    {
        public string GameId { get; set; } = "Error";
    }
}