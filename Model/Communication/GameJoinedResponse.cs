﻿using System;
using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Communication
{
    public class GameJoinedResponse : Response
    {
        public GameJoinedResponse(string gameId, List<string> userIds, string userId, List<string> playables)
        {
            GameId = gameId ?? throw new ArgumentNullException(nameof(gameId));
            UserIds = userIds ?? throw new ArgumentNullException(nameof(userIds));
            UserId = userId ?? throw new ArgumentNullException(nameof(userId));
            Playables = playables ?? throw new ArgumentNullException(nameof(playables));
        }

        public GameJoinedResponse()
        {
        }

        public string GameId { get; set; } = "invalid";

        public List<string> UserIds { get; set; }

        public string UserId { get; set; }

        public List<string> Playables { get; set; }
    }
}