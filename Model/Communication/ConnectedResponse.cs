﻿namespace ReChordNize.Backend.Model.Communication
{
    public class ConnectedResponse:Response
    {
        public string ConnectionId { get; set; } = "Error";
    }
}
