﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Communication
{
    public class NextResponse : Response
    {
        public NextResponse(int roundNumber, List<string> correctList)
        {
            RoundNumber = roundNumber;
            CorrectList = correctList;
        }

        public int RoundNumber { get; set; }
        public List<string> CorrectList { get; set; }
    }
}