﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Communication
{
    public class AccountResponse: Response
    {
        public AccountResponse(bool successFul, List<string> errorList)
        {
            SuccessFul = successFul;
            ErrorList = errorList;
        }

        public bool SuccessFul { get; set; }
        public List<string> ErrorList { get; set; }
    }
}