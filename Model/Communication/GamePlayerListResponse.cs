﻿using System.Collections.Generic;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;

namespace ReChordNize.Backend.Model.Communication
{
    public class GamePlayerListResponse : Response
    {
        public List<GamePlayer> PlayerNameList { get; set; }
    }
}