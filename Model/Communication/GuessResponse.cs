﻿namespace ReChordNize.Backend.Model.Communication
{
    public class GuessResponse : Response
    {
        public GuessResponse(string userId, bool correct)
        {
            UserId = userId;
            Correct = correct;
        }

        public GuessResponse()
        {
        }

        public string UserId { get; set; } = "Error";
        public bool Correct { get; set; }
    }
}