﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Communication
{
    public class GameEndResponse : Response
    {
        public GameEndResponse(Dictionary<string, int> userIdToScoreDictionary, string gamedId)
        {
            UserIdToScoreDictionary = userIdToScoreDictionary;
            GameId = gamedId;
        }

        public GameEndResponse()
        { }


        public string GameId { get; set; } = "error";

        public Dictionary<string, int> UserIdToScoreDictionary { get; set; }
    }
}