﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ReChordNize.Backend.Model.Models
{
    public class DailyGoal
    {
        public DailyGoal()
        {
            Description = "";
            SkillType = SkillType.Error;
            SkillValue = -1;
            Date = DateTime.MinValue;
        }

        public DailyGoal(DateTime date, string description, SkillType skillType, long skillValue)
        {
            Date = date;
            Description = description;
            SkillType = skillType;
            SkillValue = skillValue;
        }

        [Key, JsonProperty("date")] public DateTime Date { get; set; }

        [JsonProperty("description")] public string Description { get; set; }

        [JsonProperty("skillType")] public SkillType SkillType { get; set; }

        [JsonProperty("skillValue")] public long SkillValue { get; set; }

        public override string ToString() => $"DAILY GOAL  {Description} {SkillType} {SkillValue} {Date}";
    }
}