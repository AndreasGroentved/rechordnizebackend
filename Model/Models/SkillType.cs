﻿using System;
using static ReChordNize.Backend.Model.Models.SkillType;

namespace ReChordNize.Backend.Model.Models
{
    public enum SkillType
    {
        CustomLevelMade = 0,
        DaysInARow = 1,
        NumOfCorrect = 2,
        NumOfGuesses = 3,
        LongestPractise = 4,
        Level1LevelsCompleted = 5,
        Level2LevelsCompleted = 6,
        Level3LevelsCompleted = 7,
        LevelsCompleted = 8,
        IntervalMaster = 9,
        ProgressionMaster = 10,
        TypeMaster = 11,
        NameChordMaster = 12,
        Error = 13,
        TotalDays = 14,
        TotalPractiseTime = 15,
        CorrectInARow = 16 //TODO
    }

    public static class SkillTypeUtil
    {
        public static readonly int[] GetSkillTypeForDailyGoal = {0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 15 /*, 16*/};


        public static string GetDescription(SkillType skillType)
        {
            switch (skillType)
            {
                case CustomLevelMade:
                    return "Create {0} custom level(s)";
                case DaysInARow:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case NumOfCorrect:
                    return "Get {0} correct answers";
                case NumOfGuesses:
                    return "Make {0} guesses, right or wrong";
                case LongestPractise:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case Level1LevelsCompleted:
                    return "Complete {0} level(s) on normal difficulty level";
                case Level2LevelsCompleted:
                    return "Complete {0} level(s) on challenging difficulty level";
                case Level3LevelsCompleted:
                    return "Complete {0} level(s) on extreme difficulty level";
                case LevelsCompleted:
                    return "Complete {0} levels on any difficulty level";
                case IntervalMaster:
                    return "Complete {0} interval levels on any difficulty level";
                case ProgressionMaster:
                    return "Complete {0} chord progression levels on any difficulty level";
                case TypeMaster:
                    return "Complete {0} chord type levels on any difficulty level";
                case NameChordMaster:
                    return "Complete {0} chord levels on any difficulty level";
                case Error:
                case TotalDays:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case TotalPractiseTime:
                    return "Practise for {0} minutes";
                case CorrectInARow:
                    return "Get {0} correct in a row";
                default:
                    throw new ArgumentOutOfRangeException(nameof(skillType), skillType, null);
            }
        }

        public static IntPair GetValueRangeForDailySkill(SkillType skillType)
        {
            switch (skillType)
            {
                case CustomLevelMade:
                    return new IntPair(1, 3);
                case DaysInARow:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case NumOfCorrect:
                    return new IntPair(15, 50);
                case NumOfGuesses:
                    return new IntPair(20, 100);
                case LongestPractise:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case Level1LevelsCompleted:
                case Level2LevelsCompleted:
                case Level3LevelsCompleted:
                    return new IntPair(1, 3);
                case LevelsCompleted:
                case IntervalMaster:
                case ProgressionMaster:
                case TypeMaster:
                case NameChordMaster:
                    return new IntPair(2, 5);
                case Error:
                case TotalDays:
                    throw new Exception($"{nameof(skillType)} cannot be used for daily goal");
                case TotalPractiseTime:
                    return new IntPair(5, 10);
                case CorrectInARow:
                    return new IntPair(5, 20);
                default:
                    throw new ArgumentOutOfRangeException(nameof(skillType), skillType, null);
            }
        }
    }
}