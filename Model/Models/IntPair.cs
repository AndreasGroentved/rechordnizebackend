﻿namespace ReChordNize.Backend.Model.Models
{
    public class IntPair
    {
        public int First { get; set; }
        public int Second { get; set; }

        public IntPair(int first, int second)
        {
            First = first;
            Second = second;
        }
    }
}
