﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public enum GameState
    {
        UnStarted,
        Started,
        Ended
    }
}
