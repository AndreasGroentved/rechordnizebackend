﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class Round
    {
        public Round(int number, List<string> playableNames)
        {
            Number = number;
            PlayableNames = playableNames;
        }

        public int Number { get; }
        public List<string> PlayableNames { get;}
    }
}