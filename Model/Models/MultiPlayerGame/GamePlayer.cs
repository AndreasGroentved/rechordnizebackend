﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class GamePlayer
    {
        public GamePlayer(string playerName)
        {
            Name = playerName;
        }


        public string Name { get; set; }
        public int Score { get; set; } = 0;

        public State State { get; set; } = State.NotReady;
    }
}