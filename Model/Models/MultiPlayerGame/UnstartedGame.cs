﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class UnStartedGame
    {
        public UnStartedGame(LevelIdentifierEnum identifierEnum, string id)
        {
            IdentifierEnum = identifierEnum;
            Id = id;
        }

        public LevelIdentifierEnum IdentifierEnum { get; set; }
        public string Id { get; set; }
    }
}