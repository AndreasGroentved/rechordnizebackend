﻿using System.Collections.Generic;
using ReChordNize.Backend.Model.Communication;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class UnStartedGamesResponse:Response
    {
        public UnStartedGamesResponse(List<UnStartedGame> unStartedGames)
        {
            UnStartedGames = unStartedGames;
        }

        public List<UnStartedGame> UnStartedGames { get; set; }
        
    }
}