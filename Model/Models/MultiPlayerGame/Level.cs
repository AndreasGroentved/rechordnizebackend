﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class Level
    {
        public Level(LevelIdentifierEnum identifier, List<string> playables)
        {
            Identifier = identifier;
            Playables = playables;
        }

        public Level()
        {
        }

        [Key] public LevelIdentifierEnum Identifier { get; set; }
        public List<string> Playables { get; set; }
    }
}