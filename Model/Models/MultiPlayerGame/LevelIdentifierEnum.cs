﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public enum LevelIdentifierEnum
    {
        EasyChordNames = 0,
        MediumChordNames = 1,
        HardChordNames = 2,
        EasyChordTypes = 3,
        MediumChordTypes = 4,
        HardChordTypes = 5,
        EasyChordProgressions = 6,
        MediumChordProgressions = 7,
        HardChordProgressions = 8,
        EasyIntervals = 9,
        MediumIntervals = 10,
        HardIntervals = 11,
        EasyEverything = 12,
        MediumEverything = 13,
        HardEverything = 14,
    }
}