﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public enum State
    {
        NotReady = 0,
        Disconnected = 1,
        Ready = 2,
        InGame = 3
    }
}