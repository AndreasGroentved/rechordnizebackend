﻿using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class GuessInfo
    {
        public GuessInfo(List<string> guessList, string playerId, int roundNumber, int time)
        {
            GuessList = guessList;
            PlayerId = playerId;
            RoundNumber = roundNumber;
            Time = time;
        }

        public GuessInfo()
        {

        }

        public int Time { get; set; }
        public List<string> GuessList { get; set; }
        public string PlayerId { get; set; }
        public int RoundNumber { get; set; }
    }
}