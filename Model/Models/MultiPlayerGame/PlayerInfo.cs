﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class PlayerInfo
    {
        public PlayerInfo()
        {
        }


        public PlayerInfo(string id)
        {
            Id = id;

        }

        [JsonProperty("wins")]
        public int Wins { get; set; }
        [JsonProperty("id"), Key] public string Id { get; set; } = "";
    }
}