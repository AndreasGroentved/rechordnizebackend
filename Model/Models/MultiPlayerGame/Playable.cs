﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class Playable
    {
        public Playable()
        {
            Name = "error";
            Extra = PlayableExtra.Invalid;
        }

        public Playable(string name, PlayableExtra extra)
        {
            Name = name;
            Extra = extra;
        }

        [JsonProperty("name"), Key] public string Name { get; set; }

        [JsonProperty("extra")] public PlayableExtra Extra { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }
}