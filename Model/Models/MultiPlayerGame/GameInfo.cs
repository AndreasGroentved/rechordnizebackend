﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public class GameInfo
    {
        
        public string HostPlayer { get; set; }
        public LevelIdentifierEnum IdentifierEnum { get; set; }
        public DateTime StartTime { get; set; }
        public ConcurrentDictionary<string, GamePlayer> PlayerMap { get; }
        public List<string> Playables { get; }
        public List<Round> Rounds { get; } = new List<Round>();
        public GameState State { get; set; } = GameState.UnStarted;

        public GameInfo(List<string> playables, string hostPlayer, DateTime startTime,
            LevelIdentifierEnum identifier)
        {
            Playables = playables;
            PlayerMap = new ConcurrentDictionary<string, GamePlayer>();
            PlayerMap.TryAdd(hostPlayer, new GamePlayer(hostPlayer));
            StartTime = startTime;
            IdentifierEnum = identifier;
            HostPlayer = hostPlayer;
        }

        public void SetStateOfPlayer(string playerId, State state)
        {
            var foundPlayer = PlayerMap.TryGetValue(playerId, out var player);
            if(!foundPlayer) return;
            lock (player)
            {
                player.State = state;
            }
        }

    }
}