﻿namespace ReChordNize.Backend.Model.Models.MultiPlayerGame
{
    public enum PlayableExtra
    {
        Chord=0,
        Interval=9,
        ChordProgression=13,
        ChordType=14,
        Invalid=16,
        Any=18
    }
}