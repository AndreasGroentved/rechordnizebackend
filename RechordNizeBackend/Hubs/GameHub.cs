﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain.GameLogic;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ReChordNize.Backend.Model.Communication;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;


namespace ReChordNize.Backend.Web.Hubs
{
    public class GameHub : Hub
    {
        private readonly GameHandler _gameHandler;
        private const string AvailableGamesGroupName = "availableGames";

        public GameHub(GameHandler gameHandler)
        {
            _gameHandler = gameHandler;
        }

        public async Task NotifyNonRunningGames(List<UnStartedGame> l)
            => await SendMultiple(AvailableGamesGroupName, new UnStartedGamesResponse(l), AvailableGamesGroupName);


        public async Task NotifyEndGame(Response res)
        {
            var gameId = ((GameEndResponse)res).GameId;
            await SendMultiple(gameId, res, "GameEnded");
        }


        public async Task JoinGame(string gameId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, gameId);
            var res = await _gameHandler.JoinGame(gameId, Context.ConnectionId);
            await InformGroup(gameId, await _gameHandler.GetPlayersOfGame(gameId));
            await SendSingle(Context.ConnectionId, res, "JoinGame");
        }


        public async Task UnsubscribeAvailableGames()
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, AvailableGamesGroupName);
        }


        public async Task SubscribeAvailableGames()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, AvailableGamesGroupName);
            await SendSingle(Context.ConnectionId, new UnStartedGamesResponse(_gameHandler.NonRunningGames()
                .Select(g => new UnStartedGame(g.GameInfo.IdentifierEnum, g.Id)).ToList()), AvailableGamesGroupName);
        }

        public async Task JoinCreateGame(int identifier)
        {
            var connectionId = Context.ConnectionId;
            var gameId = await GetOrCreateGameId(identifier, connectionId);

            var joinGameRes = await JoinGame(connectionId, gameId);

            await SendSingle(connectionId, joinGameRes, "JoinGame");
            await InformGroup(gameId, await _gameHandler.GetPlayersOfGame(gameId));
        }

        private async Task<Response> JoinGame(string connectionId, string gameId)
        {
            var joinGameRes = await _gameHandler.JoinGame(gameId, connectionId);
            await Groups.AddToGroupAsync(connectionId, gameId);
            Context.Items.TryAdd("group", gameId);
            return joinGameRes;
        }

        private async Task<string> GetOrCreateGameId(int identifier, string connectionId)
        {
            var gameId = await _gameHandler.GetGameOfType((LevelIdentifierEnum)identifier);

            if (gameId != null) return gameId;
            gameId = Guid.NewGuid().ToString();
            await _gameHandler.CreateNonRunningGame((LevelIdentifierEnum)identifier, gameId, connectionId);

            return gameId;
        }

        private async Task SendSingle(string userId, Response response, string method)
        {
            if (response is ErrorResponse)
                await Clients.Client(userId).SendAsync("Error", JsonConvert.SerializeObject(response));
            else
                await Clients.Client(userId).SendAsync(method, JsonConvert.SerializeObject(response));
        }

        public async Task SendMultiple(string groupId, Response response, string method)
        {
            if (response is ErrorResponse)
                await Clients.Groups(groupId)
                    .SendAsync("Error", JsonConvert.SerializeObject(response)); //TODO send til alle er nok tvivlsom ide
            else
                await Clients.Groups(groupId).SendAsync(method, JsonConvert.SerializeObject(response));
        }

        public async Task InformGroup(string gameId, List<GamePlayer> userList)
        {
            if (userList == null)
            {
                Debug.WriteLine("Error, unexpected null list");
                return;
            }

            var res = new GamePlayerListResponse { PlayerNameList = userList };
            await SendMultiple(gameId, res, "PlayerMap");
        }


        public async Task StopGame(GameCancelStartResponse res)
        {
            var gameId = res.GameId;
            await SendMultiple(gameId, res, "GameCancelStart");
        }


        public async Task LeaveGame()
        {
            await RemoveFromGroup();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
            Debug.WriteLine("disconnected");
            await RemoveFromGroup();
        }


        public async Task Ready(string gameId)
        {
            //this.Context.User.Identity.Name
            await _gameHandler.ReadyPlayerFromGame(Context.ConnectionId, gameId);
            await InformGroup(gameId, await _gameHandler.GetPlayersOfGame(gameId));
        }

        public async Task UnReady(string gName)
        {
            if (_gameHandler.IsRunningGame(gName)) return;
            await _gameHandler.UnReadyPlayerFromGame(Context.ConnectionId, gName);
            await InformGroup(gName, await _gameHandler.GetPlayersOfGame(gName));
        }

        private async Task RemoveFromGroup()
        {
            Context.Items.TryGetValue("group", out var groupObject);
            if (groupObject is string gName)
            {
                if (_gameHandler.IsRunningGame(gName)) return;
              
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, gName);
                var res = await _gameHandler.LeaveGame(gName, Context.ConnectionId);
                
                await InformGroup(gName, (res as GamePlayerListResponse)?.PlayerNameList);
            }
        }


        public async Task Next(string gameId, int roundNumber)
        {
            var res = await _gameHandler.Next(roundNumber, gameId);
            await SendSingle(Context.ConnectionId, res, "Next");
        }


        public async Task Guess(string gameId, string guessInfoString)
        {
            var guessInfo = JsonConvert.DeserializeObject<GuessInfo>(guessInfoString);
            guessInfo.PlayerId = Context.ConnectionId;
            var res = await _gameHandler.Guess(guessInfo, gameId);

            await SendSingle(Context.ConnectionId, res, "Guess");
        }


        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
            await Clients.Clients(Context.ConnectionId).SendAsync("connected", JsonConvert.SerializeObject(new ConnectedResponse { ConnectionId = Context.ConnectionId }));
        }
    }
}