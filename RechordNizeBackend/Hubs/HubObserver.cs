﻿using System;
using System.Diagnostics;
using Domain.GameLogic;
using ReChordNize.Backend.Model.Communication;


namespace ReChordNize.Backend.Web.Hubs
{
    public class HubObserver
    {
        private readonly GameHandler _gameHandler;
        private readonly GameHub _gameHub;

        public HubObserver(GameHandler gameHandler, GameHub gameHub)
        {
            _gameHub = gameHub;
            _gameHandler = gameHandler;
            StartObserving();
        }

        private void StartObserving()
        {
            ObserverRunningGames();
            ObserverEndGame();
            ObserveGameStart();
            ObserveGameTimeStartCancel();
        }


        private void ObserveGameTimeStartCancel()
        {
            _gameHandler.StartGameObserver.Subscribe(async res =>
            {
                if (res is GameStartedResponse gameStarted)
                    await _gameHub.SendMultiple(gameStarted.GameId, gameStarted, "StartGame");
            });
        }


        private void ObserveGameStart()
        {
            _gameHandler.StartStopGameTimer.Subscribe(async res =>
            {
                if (res is GameIsStartingResponse gameStarted)
                    await _gameHub.SendMultiple(gameStarted.GameId, gameStarted, "GameIsStarting");
                else if (res is GameCancelStartResponse gameCancelled)
                    await _gameHub.SendMultiple(gameCancelled.GameId, gameCancelled, "GameCancelStart");
                else
                    Debug.WriteLine("Anomaly...");
            });
        }

        private void ObserverRunningGames()
            => _gameHandler.NonRunningGamesSubject.Subscribe(async unStartedGames => await _gameHub.NotifyNonRunningGames(unStartedGames));


        private void ObserverEndGame()
            => _gameHandler.GameEndedSubject.Subscribe(async response => await _gameHub.NotifyEndGame(response));

    }
}