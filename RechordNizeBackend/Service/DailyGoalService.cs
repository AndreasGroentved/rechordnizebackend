﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Model.Models;
using static System.String;

namespace ReChordNize.Backend.Web.Service
{
    public class DailyGoalService
    {
        private readonly SemaphoreSlim _createSemaphore;
        private readonly ReChordNizeContext _dailyContext;
        private readonly Random _random = new Random();
        private readonly IConfiguration con;

        public DailyGoalService(ReChordNizeContext dbContext,IConfiguration con)
        {
            _dailyContext = dbContext;
            _createSemaphore = new SemaphoreSlim(1, 1);
            this.con = con;
        }

        //TODO begræns bagud i tid
        public async Task<DailyGoal> GetDailyGoal(DateTime today, DateTime? now = null)
        {
            Debug.WriteLine("Con");
            Debug.WriteLine(con.GetConnectionString("rechordnizebackend"));
            if (now == null) now = DateTime.Now;
            today = (today - now.Value).TotalDays > 1 ? DateTime.Today : ResetToMidnight(today);
            var dailyGoal = await GetDbGoal(today);
            if (dailyGoal != null) return dailyGoal;

            return await CreateAndGetNewGoal(today);
        }

        private async Task<DailyGoal> CreateAndGetNewGoal(DateTime today)
        {
            await _createSemaphore.WaitAsync();

            if (await GetDbGoal(today) == null)
                await GenerateNewDailyGoalAndAddToDb(today);

            _createSemaphore.Release();
            return await GetDbGoal(today);
        }

        private async Task GenerateNewDailyGoalAndAddToDb(DateTime today)
        {
            var dailyGoal = await GenerateDailyGoal(today);
            await _dailyContext.DailyGoals.AddAsync(dailyGoal);
            await _dailyContext.SaveChangesAsync();
        }

        private DateTime ResetToMidnight(DateTime date) =>
            new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);

        private async Task<DailyGoal> GetDbGoal(DateTime today) =>
            (await _dailyContext.DailyGoals.Where(a => a.Date.Equals(today)).ToListAsync()).FirstOrDefault();


        private async Task<DailyGoal> GenerateDailyGoal(DateTime today)
        {
            var lastFiveDays = await GetLastFiveDays();

            var possible = SkillTypeUtil.GetSkillTypeForDailyGoal;
            var index = GetRandomNewIndex(possible, lastFiveDays);

            var dailyGoal = CreateDailyGoalObject(today, possible, index);
            return dailyGoal;
        }

        private DailyGoal CreateDailyGoalObject(DateTime today, int[] possible, int index)
        {
            var todaySkill = (SkillType) possible[index];
            var valuePair = SkillTypeUtil.GetValueRangeForDailySkill(todaySkill);
            var value = _random.Next(valuePair.First, valuePair.Second);
            var text = Format(SkillTypeUtil.GetDescription(todaySkill), value);
            return new DailyGoal(today, text, todaySkill, value);
        }

        private async Task<List<DailyGoal>> GetLastFiveDays()
        {
            var last5DaysOrNumberOfGeneratedDailyGoals = Math.Min(5, await _dailyContext.DailyGoals.CountAsync());

            if (last5DaysOrNumberOfGeneratedDailyGoals == 0)
                return new List<DailyGoal>();

            return await _dailyContext.DailyGoals.OrderByDescending(a => a.Date)
                .Take(last5DaysOrNumberOfGeneratedDailyGoals).ToListAsync();
        }

        private int GetRandomNewIndex(IReadOnlyList<int> possible, IReadOnlyCollection<DailyGoal> lastFiveDays)
        {
            var index = _random.Next(possible.Count);
            if (!lastFiveDays.Any()) return index;

            while (lastFiveDays.Any(a => (int) a.SkillType == possible[index]))
                index = _random.Next(possible.Count);

            return index;
        }
    }
}