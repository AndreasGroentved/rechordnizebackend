using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Web.Hubs;
using ReChordNize.Backend.Web.Service;
using Domain.GameLogic;
using Domain.Time;
using Microsoft.AspNetCore.SignalR;


namespace ReChordNize.Backend.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public static readonly SymmetricSecurityKey SecurityKey = new SymmetricSecurityKey(Guid.NewGuid().ToByteArray());


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("rechordnizebackend")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                    options.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+/")
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            Debug.WriteLine("Connection");
            Debug.WriteLine(Configuration.GetConnectionString("rechordnizebackend"));
    
            services.AddDbContext<ReChordNizeContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("rechordnizebackend")));


            services.AddSingleton<ILevelRepo, LevelRepo>();
            services.AddScoped<DailyGoalService, DailyGoalService>(); //TODO brug repo
            services.AddSingleton<ITimeProvider, DateTimeWrapper>();
            services.AddSingleton<GameHandler, GameHandler>();
            services.AddSingleton<HubObserver, HubObserver>();
            services.AddSingleton<Hubs.GameHub, Hubs.GameHub>();
            services.AddSingleton<IUserIdProvider, EmailBasedUserIdProvider>();
            //services.Add

            services.AddAuthentication(options =>
                {
                    // Identity made Cookie authentication the default.
                    // However, we want JWT Bearer Auth to be the default.
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    // Configure JWT Bearer Auth to expect our security key
                    options.TokenValidationParameters =
                        new TokenValidationParameters
                        {
                            LifetimeValidator =
                                (before, expires, token, param) => { return expires > DateTime.UtcNow; },
                            ValidateAudience = false,
                            ValidateIssuer = false,
                            ValidateActor = false,
                            ValidateLifetime = true,
                            IssuerSigningKey = SecurityKey,
                        };

                    // We have to hook the OnMessageReceived event in order to
                    // allow the JWT authentication handler to read the access
                    // token from the query string when a WebSocket or 
                    // Server-Sent Events request comes in.
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                path.StartsWithSegments("/game"))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                                
                            }

                            return Task.CompletedTask;
                        },
                    };
                });


            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
            });

            services.AddSignalR();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //services.AddScoped<TokenHandler, TokenHandler>();

            // services.AddAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc();
            app.UseSignalR(routes => { routes.MapHub<Hubs.GameHub>("/game"); });
        }
    }
}