﻿namespace ReChordNize.Backend.Web.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Model.Models;
    using Service;

    [Route("api/[controller]")]
    [ApiController]
    public class DailyGoalController : ControllerBase
    {
        private readonly DailyGoalService _goalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyGoalController"/> class.
        /// Controller for getting a daily goal.
        /// </summary>
        public DailyGoalController(DailyGoalService goalService)
        {
            _goalService = goalService;
        }


        /// <summary>
        /// Returns a goal for the current day.
        /// </summary>
        /// <returns>A <see cref="Task{JsonResult}"/> JSON conversion of <see cref="DailyGoal"/>.</returns>
        [HttpGet]
        public async Task<JsonResult> Get()
        {
            var date = DateTime.Today;
            return new JsonResult(await _goalService.GetDailyGoal(date));
        }


        /// <summary>
        ///  Returns a goal for a specific day.
        /// </summary>
        /// <param name="dateString">Date formatted as "yyyymmdd"</param>
        /// <returns>A <see cref="Task{TResult}"/> JSON conversion of <see cref="DailyGoal"/>.</returns>
        [HttpGet("{dateString}")]
        public async Task<ActionResult<DailyGoal>> GetByDate(string dateString)
        {
            var date = DateTime.ParseExact(dateString, "yyyyMMdd", null);
            return await _goalService.GetDailyGoal(date);
        }
    }
}