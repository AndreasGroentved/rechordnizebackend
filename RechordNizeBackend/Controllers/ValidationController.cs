﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace ReChordNize.Backend.Web.Controllers
{
    public class ValidationController
    {
        private readonly JwtSecurityTokenHandler _tokenHandler;
        private readonly SymmetricSecurityKey _key;

        public ValidationController(JwtSecurityTokenHandler jwtSecurityTokenHandler, SymmetricSecurityKey key)
        {
            _tokenHandler = jwtSecurityTokenHandler;
            this._key = key;
        }


        public ClaimsIdentity GetIdentify(HttpRequest request)
        {
            if (!request.Headers.ContainsKey("Authorization")) return null;
            var token = ExtractTokenFromBearerString(request);
            var principal = GetPrincipal(token);

            return (ClaimsIdentity) principal?.Identity;
        }

        private string ExtractTokenFromBearerString(HttpRequest request)
        {
            string bearerToken = request.Headers["Authorization"];
            var token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;
            return token;
        }


        /// <summary>
        /// Returns ClaimsPrincipal if token is valid, otherwise null is returned
        /// </summary>
        public ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                return ValidateToken(token);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }

        private ClaimsPrincipal ValidateToken(string token)
        {
            return _tokenHandler.ReadToken(token) is JwtSecurityToken
                ? _tokenHandler.ValidateToken(token, CreateParameters(), out _)
                : null;
        }

        private TokenValidationParameters CreateParameters()
            => new TokenValidationParameters
            {
                RequireExpirationTime = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = _key
            };
    }
}