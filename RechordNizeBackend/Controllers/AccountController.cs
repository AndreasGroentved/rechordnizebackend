using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using ReChordNize.Backend.Database;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;


namespace ReChordNize.Backend.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private  readonly SigningCredentials _signingCredentials;
           

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private readonly JwtSecurityTokenHandler _tokenHandler = new JwtSecurityTokenHandler();
        private readonly SymmetricSecurityKey _key;

        public AccountController(SignInManager<ApplicationUser> signInManager, ILogger logger,
            UserManager<ApplicationUser> userManager, SymmetricSecurityKey key)
        {
            _signInManager = signInManager;
            _logger = logger;
            _userManager = userManager;
            _signingCredentials =  new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out");
            return JsonConvert.SerializeObject(new {res = "logged out"});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> LoggedInTest()
        {
            return JsonConvert.SerializeObject(new {res = ControllerContext.HttpContext.User.Identity});
        }

        [HttpPost]
        public async Task<string> LoggedInTest2()
        {
            //return GetIdentify();

            return JsonConvert.SerializeObject(new {res = ""});
        }


        [HttpPost]
        public async Task<ActionResult<bool>> Register(string email, string password, string userName)
        {
            var errors = ValidateInputForRegister(email, password, userName);

            if (errors.Any())
                return NotFound(errors);

            var userCreationResult = await CreateUser(email, password, userName);

            if (!userCreationResult.Succeeded) //DBConstraints failed, e.g. user already exists
                return BadRequest("Input is invalid"); //TODO mere specific error
            return userCreationResult.Succeeded;
        }

        private async Task<IdentityResult> CreateUser(string email, string password, string userName)
        {
            var user = new ApplicationUser {UserName = userName, Email = email};
            var result = await _userManager.CreateAsync(user, password);
            return result;
        }

        //TODO mere udf�rlige checks
        private static List<string> ValidateInputForRegister(string email, string password, string userName)
        {
            var errors = new List<string>();
            if (string.IsNullOrEmpty(email)) errors.Add("Invalid email");
            if (string.IsNullOrEmpty(password)) errors.Add("Invalid password");
            if (string.IsNullOrEmpty(userName)) errors.Add("Invalid user name");
            return errors;
        }


        [HttpPost]
        public async Task<string> Login(string email, string password)
        {
            try
            {
                var user = await GetUser(email, password);
                return await CreateTokenForUser(user);
            }
            catch (Exception ex)
            {
                return GetErrorMessage(ex.ToString());
            }
        }

        private async Task<ApplicationUser> GetUser(string email, string password)
        {
            var user = await _signInManager.UserManager.FindByEmailAsync(email);
            if (user == null)
                throw new ArgumentException("Login failed");
            if ((await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure: false)).Succeeded)
                return user;
            throw new ArgumentException("Login failed");
        }

        private async Task<string> CreateTokenForUser(ApplicationUser user)
        {
            var token = await CreateToken(user);
            return JsonConvert.SerializeObject(new
            {
                token = _tokenHandler.WriteToken(token)
            });
        }

        private static string GetErrorMessage(string error)
            => JsonConvert.SerializeObject(new {error});


        private async Task<JwtSecurityToken> CreateToken(ApplicationUser user)
        {
            var principal = await _signInManager.CreateUserPrincipalAsync(user);
            return new JwtSecurityToken(
                "SignalRAuthenticationSample", "SignalRAuthenticationSample", principal.Claims,
                expires: DateTime.UtcNow.AddDays(30), signingCredentials: _signingCredentials);
        }
    }
}