﻿using System.Security.Claims;
using Microsoft.AspNetCore.SignalR;

namespace ReChordNize.Backend.Web
{
    public class EmailBasedUserIdProvider : IUserIdProvider
    {
        public virtual string GetUserId(HubConnectionContext connection) => 
            connection?.User?.FindFirst(ClaimTypes.Email)?.Value;
    }
}