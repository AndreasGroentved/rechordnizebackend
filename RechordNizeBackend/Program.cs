using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Web.Hubs;

namespace ReChordNize.Backend.Web
{
    public class Program
    {

        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            InitStartData(host);
            host.Run();
        }

        private static void InitStartData(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                EnsureDbsAreCreated(services);
                StartHubObserver(services);
            }
        }

        private static void StartHubObserver(IServiceProvider services)
        {
            services.GetRequiredService<HubObserver>();
        }

        private static async void EnsureDbsAreCreated(IServiceProvider services)
        {
            try
            {
                EnsureDailyIsCreated(services);
                EnsureLevelsAreCreated(services);
                EnsurePlayablesAreCreated(services);
                await EnsureUserDbIsCreated(services);
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "An error occurred while seeding the database.");
                //throw;

            }
        }

        private static void EnsurePlayablesAreCreated(IServiceProvider services)
        {
            var playableContext = services.GetRequiredService<ReChordNizeContext>();
            PlayableIni.Initialize(playableContext);
        }

        private static void EnsureLevelsAreCreated(IServiceProvider services)
        {
            var levelContext = services.GetRequiredService<ReChordNizeContext>();
            LevelIni.Initialize(levelContext);
        }

        private static void EnsureDailyIsCreated(IServiceProvider services)
        {
            var dailyGoalContext = services.GetRequiredService<ReChordNizeContext>();
            DailyGoalIni.Initialize(dailyGoalContext);
        }

        private static async Task EnsureUserDbIsCreated(IServiceProvider services)
        {
            try
            {
                var context = services.GetRequiredService<ApplicationDbContext>();
                if (!context.Database.EnsureCreated()) return;
                var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                await AppIdentityDbContextSeed.SeedAsync(userManager);
            }
            catch (Exception ex)
            {
                services.GetRequiredService<ILogger<Program>>()
                    .LogError(ex, "An error occurred while seeding the database.");
                throw;
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}