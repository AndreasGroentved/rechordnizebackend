﻿using System.Collections.Generic;
using System.Linq;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using ReChordNize.Backend.Model.Util;

namespace ReChordNize.Backend.Database
{
    public static class PlayableDataBaseCreationData
    {
        public static IEnumerable<Playable> GetIntervals()
        {
            var intervals = ChordUtil.IntervalNames;
            return intervals.Select(name => new Playable(name, PlayableExtra.Interval));
        }

        public static IEnumerable<Playable> GetTypes()
        {
            var types = ChordUtil.TypeNames;
            return types.Select(name => new Playable(name, PlayableExtra.ChordType));
        }


        public static IEnumerable<Playable> GetProgressions()
        {
            var allProgressions = ChordUtil.ProgressionNames;
            return AddTypesToRoots(allProgressions, PlayableExtra.ChordProgression);
        }

        public static IEnumerable<Playable> GetNames()
        {
            var roots = ChordUtil.NoteNames;
            return AddTypesToRoots(roots, PlayableExtra.Chord);
        }

        private static IEnumerable<Playable> AddTypesToRoots(IEnumerable<string> roots, PlayableExtra playableExtra)
        {
            var typesNames = ChordUtil.TypeNames;
            return roots.SelectMany(rootName =>
                typesNames.Select(typeName => $"{rootName} {typeName}")
                    .Select(fullName => new Playable(fullName, playableExtra)));
        }
    }
}