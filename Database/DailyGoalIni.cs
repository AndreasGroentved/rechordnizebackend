﻿using System;

namespace ReChordNize.Backend.Database
{
    public static class DailyGoalIni
    {
        public static void Initialize(ReChordNizeContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            context.Database.EnsureCreated();
        }
    }
}