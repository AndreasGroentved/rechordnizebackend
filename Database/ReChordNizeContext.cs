﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ReChordNize.Backend.Model.Models;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;

namespace ReChordNize.Backend.Database
{
    public class ReChordNizeContext : DbContext
    {
        public ReChordNizeContext(DbContextOptions options) : base(options)
        {
        }

        public ReChordNizeContext()
        {
        }

        public DbSet<DailyGoal> DailyGoals { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<PlayerInfo> Players { get; set; }
        public DbSet<Playable> Playables { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null) throw new ArgumentNullException(nameof(modelBuilder));

            modelBuilder.Entity<PlayerInfo>().ToTable("Players");
            modelBuilder.Entity<DailyGoal>().ToTable("DailyGoal");
            modelBuilder.Entity<Playable>().ToTable("Playable")
                .Property(e => e.Name)
                .HasConversion(
                    v => AddUnderScoreForFirstUpper(v),
                    v => RemoveUnderScoreForFirstUpper(v));
            modelBuilder.Entity<Level>().ToTable("Level")
                .Property(e => e.Playables)
                .HasConversion(
                    v => string.Join(',', v),
                    v => v.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList());
        }

        private static string AddUnderScoreForFirstUpper(string name)
            => char.IsUpper(name.First()) ? $"_{name}" : name;

        public static string RemoveUnderScoreForFirstUpper(string name) => 
            name.First() == '_' ? name.Replace("_", "").FirstLetterToUpperCase() : name;
    }
}