﻿using System.Collections.Generic;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using System.Linq;

namespace ReChordNize.Backend.Database
{
    public static class PlayableIni
    {
        public static void Initialize(ReChordNizeContext context)
        {
            context.Database.EnsureCreated();

            if (context.Playables.Any()) return; // DB has been seeded

            var playables = GetAllPlayables();
            context.Playables.AddRange(playables);
            context.SaveChanges();
        }

        private static List<Playable> GetAllPlayables()
        {
            var playables = new List<Playable>();
            playables.AddRange(PlayableDataBaseCreationData.GetTypes());
            playables.AddRange(PlayableDataBaseCreationData.GetIntervals());
            playables.AddRange(PlayableDataBaseCreationData.GetProgressions());
            playables.AddRange(PlayableDataBaseCreationData.GetNames());
            return playables;
        }
    }
}