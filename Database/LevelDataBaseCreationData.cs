﻿using System.Collections.Generic;
using System.Linq;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;

namespace ReChordNize.Backend.Database
{
    public static class LevelDataBaseCreationData
    {
        public static List<Level> GetIntervals()
        {
            var easyIntervals = new Level(LevelIdentifierEnum.EasyIntervals, IntervalEasy().ToList());
            var mediumIntervals = new Level(LevelIdentifierEnum.MediumIntervals, IntervalMedium().ToList());
            var hardIntervals = new Level(LevelIdentifierEnum.HardIntervals, IntervalHard().ToList());
            return new List<Level> {easyIntervals, mediumIntervals, hardIntervals};
        }

        public static List<Level> GetTypes()
        {
            var easyTypes = new Level(LevelIdentifierEnum.EasyChordTypes, TypeEasy().ToList());
            var mediumTypes = new Level(LevelIdentifierEnum.MediumChordTypes, TypeMedium().ToList());
            var hardTypes = new Level(LevelIdentifierEnum.HardChordTypes, TypeHard().ToList());
            return new List<Level> {easyTypes, mediumTypes, hardTypes};
        }


        public static List<Level> GetProgressions()
        {
            var easyProgressions = new Level(LevelIdentifierEnum.EasyChordProgressions, ProgressionEasy().ToList());
            var mediumProgressions =
                new Level(LevelIdentifierEnum.MediumChordProgressions, ProgressionMedium().ToList());
            var hardProgressions = new Level(LevelIdentifierEnum.HardChordProgressions, ProgressionHard().ToList());
            return new List<Level> {easyProgressions, mediumProgressions, hardProgressions};
        }

        public static List<Level> GetNames()
        {
            var easyNames = new Level(LevelIdentifierEnum.EasyChordNames, NameEasy().ToList());
            var mediumNames = new Level(LevelIdentifierEnum.MediumChordNames, NameMedium().ToList());
            var hardNames = new Level(LevelIdentifierEnum.HardChordNames, NameHard().ToList());
            return new List<Level> {easyNames, mediumNames, hardNames};
        }

        public static List<Level> GetMixed()
        {
            var playablesEasy = AllPlayablesEasy();
            var playablesMedium = AllPlayablesMedium();
            var playablesHard = AllHardPlayables();

            return new List<Level>
            {
                new Level(LevelIdentifierEnum.EasyEverything, playablesEasy),
                new Level(LevelIdentifierEnum.MediumEverything, playablesMedium),
                new Level(LevelIdentifierEnum.HardEverything, playablesHard)
            };
        }

        private static List<string> AllPlayablesEasy()
        {
            var playablesEasy = new List<string>();
            playablesEasy.AddRange(NameEasy());
            playablesEasy.AddRange(TypeEasy());
            playablesEasy.AddRange(ProgressionEasy());
            playablesEasy.AddRange(IntervalEasy());
            return playablesEasy;
        }

        private static List<string> AllPlayablesMedium()
        {
            var playablesMedium = new List<string>();
            playablesMedium.AddRange(NameMedium());
            playablesMedium.AddRange(TypeMedium());
            playablesMedium.AddRange(ProgressionMedium());
            playablesMedium.AddRange(IntervalMedium());
            return playablesMedium;
        }

        private static List<string> AllHardPlayables()
        {
            var playablesHard = new List<string>();
            playablesHard.AddRange(NameHard());
            playablesHard.AddRange(TypeHard());
            playablesHard.AddRange(ProgressionHard());
            playablesHard.AddRange(IntervalHard());
            return playablesHard;
        }

        private static List<string> IntervalEasy() => new List<string>
        {
            "A4", "M2", "M3", "P1", "P4", "P5", "m2", "m3", "m6"
        };

        private static List<string> IntervalMedium() => new List<string>
        {
            "A4", "M6", "M7", "M9", "M10", "M2", "M3", "P8", "P11", "P1", "P4", "P5", "m7", "m9", "m10", "m2", "m3",
            "m6"
        };

        private static List<string> IntervalHard() => new List<string>
        {
            "A4", "A11", "M6", "M7", "M9", "M10", "M2", "M3", "M13", "M14", "P8", "P11", "P1", "P4", "P5", "P12", "m7",
            "m9", "m10", "m2", "m3", "m6", "m13", "m14"
        };

        private static List<string> TypeEasy() => new List<string>
        {
            "maj", "min", "dim", "7", "maj7", "min7", "dim7", "b5"
        };

        private static List<string> TypeMedium() => new List<string>
        {
            "sus2", "sus4", "sus2sus4", "5", "6", "6/9", "min#5", "aug", "min6", "7b5", "maj", "min", "dim", "7",
            "maj7", "min7", "dim7", "b5"
        };

        private static List<string> TypeHard() => new List<string>
        {
            "min7b5", "aug7", "minmaj7", "9", "min9", "11", "11", "min11", "13", "min13", "sus2", "sus4", "sus2sus4",
            "5", "6", "6/9", "min#5", "aug", "min6", "7b5", "maj", "min", "dim", "7", "maj7", "min7", "dim7", "b5"
        };

        private static List<string> ProgressionEasy() => new List<string>
        {
            "I maj", "IV maj", "II min", "III min", "V maj", "VI min", "VII dim"
        };

        private static List<string> ProgressionMedium() => new List<string>
        {
            "I maj7", "IV maj7", "II min7", "III min7", "I maj", "IV maj", "II min", "III min", "V maj7", "VI min7",
            "VII dim7", "V maj", "VI min", "VII dim"
        };

        private static List<string> ProgressionHard() => new List<string>
        {
            "I maj", "IV maj", "II min", "III min", "II 7", "I maj7", "IV maj7", "II min7", "III min7", "II dim",
            "V maj", "VI min", "VII dim", "V maj7",
            "VI min7", "VII dim7", "V 7", "VI 7", "bVI 7", "bII dim", "bV 7", "bV min7", "bVI min7", "bIII maj7",
            "bV maj7", "bIII dim", "bII maj7", "bIII min7", "bII 7"
        };

        private static List<string> NameEasy() => new List<string>
        {
            "A 7", "A maj7", "A min7", "A maj", "A min", "A sus2", "A sus4", "B 7", "C 7", "C maj7", "C maj", "C sus2",
            "D min7", "D maj", "D min",
            "D sus2", "D sus4", "E 7", "E min7", "E maj", "E min", "F 7", "F maj7", "F maj", "G 7", "G maj", "G sus2"
        };

        private static List<string> NameMedium() => new List<string>
        {
            "A 5", "Ab 5", "B 5", "Bb 5", "A 7", "A maj7", "A min7", "A maj", "A min", "A sus2", "A sus4", "C 5", "B 7",
            "D 5", "Db 5", "C 7", "C maj7", "C maj", "C sus2",
            "E 5", "Eb 5", "D min7", "D maj", "D min", "D sus2", "D sus4", "F 5", "E 7", "E min7", "E maj", "E min",
            "Gb 5", "G 5", "F 7", "F maj7", "F maj", "G 7", "G maj", "G sus2"
        };

        private static List<string> NameHard() => new List<string>
        {
            "A 5", "Ab 5", "B 5", "Bb 5", "A 7", "A maj7", "A min7", "A maj", "A min", "A sus2", "A sus4", "C 5", "B 7",
            "D 5", "Db 5", "C 7", "C maj7",
            "C maj", "C sus2", "E 5", "Eb 5", "D min7", "D maj", "D min", "D sus2", "D sus4", "F 5", "E 7", "E min7",
            "E maj", "E min", "Gb 5", "G 5", "F 7",
            "F maj7", "F maj", "G 7", "G maj", "G sus2", "A 7", "Ab 7", "A maj7", "Ab maj7", "A min7", "Ab min7",
            "A min", "Ab min", "A maj", "Ab maj", "B 7",
            "Bb 7", "B maj7", "Bb maj7", "B min7", "Bb min7", "B min", "Bb min", "B maj", "Bb maj", "C 7", "C maj7",
            "C min7", "C min", "C maj", "D 7", "Db 7",
            "D maj7", "Db maj7", "D min7", "Db min7", "D min", "Db min", "D maj", "Db maj", "E 7", "Eb 7", "E maj7",
            "Eb maj7", "E min7", "Eb min7", "E min",
            "Eb min", "E maj", "Eb maj", "F 7", "F maj7", "F min7", "F min", "F maj", "Gb 7", "G 7", "Gb maj7",
            "G maj7", "Gb min7", "G min7", "Gb min", "G min", "Gb maj", "G maj"
        };
    }
}