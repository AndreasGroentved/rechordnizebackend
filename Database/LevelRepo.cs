﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;

namespace ReChordNize.Backend.Database
{
    public class LevelRepo : ILevelRepo
    {
        private readonly IServiceProvider _iServiceProvider;

        //Jeg er ikke helt vild med denne dependency, men dbcontext skal være scoped, TODO find ud af bedre løsning
        public LevelRepo(IServiceProvider iServiceProvider)
        {
            _iServiceProvider = iServiceProvider;
        }


        //Override
        public async Task<Level> GetLevelWithIdentifier(LevelIdentifierEnum levelIdentifier)
        {
            Level level;
            
            using (var scope = _iServiceProvider.CreateScope()) //TODO kan man kalde new?
            {
                var levelContext = scope.ServiceProvider.GetRequiredService<ReChordNizeContext>();
                level = await levelContext.Levels
                    .Where(a => a.Identifier == levelIdentifier)
                    .FirstAsync();
            }
            return level;
        }
    }


}