﻿namespace ReChordNize.Backend.Database
{
    public static class PlayerIni
    {
        public static void Initialize(ReChordNizeContext context) => context.Database.EnsureCreated();
    }
}