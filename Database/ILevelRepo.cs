﻿using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using System.Threading.Tasks;

namespace ReChordNize.Backend.Database
{
    public interface ILevelRepo
    {
        Task<Level> GetLevelWithIdentifier(LevelIdentifierEnum levelIdentifier);
    }
}
