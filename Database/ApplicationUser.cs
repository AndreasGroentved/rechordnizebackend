using Microsoft.AspNetCore.Identity;

namespace ReChordNize.Backend.Database
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
