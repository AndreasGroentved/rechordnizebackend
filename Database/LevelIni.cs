﻿using System;
using System.Collections.Generic;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using System.Linq;

namespace ReChordNize.Backend.Database
{
    public static class LevelIni
    {
        public static void Initialize(ReChordNizeContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            context.Database.EnsureCreated();

            if (context.Levels.Any()) return; //Intet at se her

            var levels = GetAllLevels();

            context.Levels.AddRange(levels);
            context.SaveChanges();
        }

        private static List<Level> GetAllLevels()
        {
            var levels = new List<Level>();
            levels.AddRange(LevelDataBaseCreationData.GetTypes());
            levels.AddRange(LevelDataBaseCreationData.GetIntervals());
            levels.AddRange(LevelDataBaseCreationData.GetProgressions());
            levels.AddRange(LevelDataBaseCreationData.GetNames());
            levels.AddRange(LevelDataBaseCreationData.GetMixed());
            return levels;
        }
    }
}