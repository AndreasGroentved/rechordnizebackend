﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ReChordNize.Backend.Database
{
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager)
        {
            if (userManager == null) throw new ArgumentNullException(nameof(userManager));

            var defaultUser = new ApplicationUser { UserName = "a@a.dk", Email = "a@a.dk" };
            await userManager.CreateAsync(defaultUser, "_hurtigtL0gin");//TODO TODO TODO
        }
    }
}
