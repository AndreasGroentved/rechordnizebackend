﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Model.Models;
using ReChordNize.Backend.Web.Service;
using Xunit;

namespace ReChordNizeTest
{
    public class DailyGoalServiceTest
    {
        private readonly DailyGoalService _dailyGoalService;
        private readonly ReChordNizeContext _db;

        public DailyGoalServiceTest()
        {
            var dbOptions = new DbContextOptionsBuilder<ReChordNizeContext>()
                .UseInMemoryDatabase("TestDaily")
                .Options;

            _db = new ReChordNizeContext(dbOptions);
            //_dailyGoalService = new DailyGoalService(_db);
        }

        //TODO setup teardown

        [Fact]
        public void UniqueDateConstraint()
        {
            var date = DateTime.Now;
            var daily = new DailyGoal { Date = date };
            var daily2 = new DailyGoal { Description = "different", Date = date };
            foreach (var dbDailyGoal in _db.DailyGoals)
                _db.DailyGoals.Remove(dbDailyGoal);

            _db.DailyGoals.Add(daily);

            Assert.Throws<InvalidOperationException>(() => _db.DailyGoals.Add(daily2));
        }


        [Fact]
        public async Task GetDailyGoal() //TODO overvej om det ikke skal være 3 tests
        {
            var now = DateTime.Today;
            var firstDate = new DateTime(now.Year, now.Month, now.Day, 1, 5, 5, 2); //Ensure midnight

            var dailyGoal1 = await _dailyGoalService.GetDailyGoal(firstDate);

            var dateTimeNow = firstDate.AddHours(5);
            var secondDate = firstDate.AddDays(1);
            var thirdDate = secondDate.AddDays(4);

            var dailyGoal2 = await _dailyGoalService.GetDailyGoal(secondDate, dateTimeNow);
            var dailyGoal3 = await _dailyGoalService.GetDailyGoal(thirdDate, dateTimeNow.AddHours(-5));
            var dailyGoal4 = await _dailyGoalService.GetDailyGoal(thirdDate.AddDays(1), dateTimeNow.AddHours(-5));

            Assert.NotEqual(dailyGoal1.Date, dailyGoal2.Date);
            Assert.Equal(dailyGoal1.Date, dailyGoal3.Date);
            Assert.Equal(dailyGoal1.Date, dailyGoal4.Date);

            Assert.True(_db.DailyGoals.ToList().All(a => IsMidnight(a.Date)), "all dates are 0'ed to midnight");
        }

        private bool IsMidnight(DateTime date) =>
            date.Hour == 0 && date.Second == 0 && date.Millisecond == 0 && date.Minute == 0;
    }
}