﻿using System.Diagnostics;
using System.Linq;
using ReChordNize.Backend.Database;
using Xunit;
using static System.Char;

namespace ReChordNizeTest.Database
{
    public class PlayableDataBaseCreationDataTest
    {
        [Fact]
        public void GetIntervals()
        {
            var names = PlayableDataBaseCreationData.GetIntervals().ToList();
            const int expected = 25;
            Assert.Equal(expected, actual: names.Count); // "Generated expected count of playables"
            Assert.True(names.All(a => Enumerable.Range(2, 3).Contains(a.Name.Length) && IsDigit(a.Name.Last())),
                "Check format of name output");
            Assert.True(names.Distinct().Count() == names.Count,"no duplicates");
        }

        [Fact]
        public void GetTypes()
        {
            var names = PlayableDataBaseCreationData.GetTypes().ToList();
            const int expected = 28;
            Assert.Equal(expected, names.Count); // "Generated expected count of playables"
            Assert.True(names.Any(a => a.Name == "min"), "Check format of name output");
            Assert.True(names.Distinct().Count() == names.Count,"no duplicates");
        }

        [Fact]
        public void GetProgressions()
        {
            var names = PlayableDataBaseCreationData.GetProgressions().ToList();
            const int expected = 12 /*steps*/ * 28;
            Assert.Equal(expected, names.Count); // "Generated expected count of playables"
            Assert.True(names.All(a => a.Name.Split(" ").Length == 2),
                "Check format of name output");
            Assert.True(names.Distinct().Count() == names.Count,"no duplicates");
        }

        [Fact]
        public void GetNames()
        {
            var names = PlayableDataBaseCreationData.GetNames().ToList();
            var expected = 12 /*Note names*/ * 28 /*Types*/;
            Assert.Equal(expected, names.Count); // "Generated expected count of playables"
            Assert.True(names.All(a => IsUpper(a.Name.First()) && a.Name.Split(" ").Length == 2),
                "Check format of name output");
            Assert.True(names.Distinct().Count() == names.Count,"no duplicates");
        }
    }
}