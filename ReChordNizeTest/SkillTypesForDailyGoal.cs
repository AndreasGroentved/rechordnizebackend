using NUnit.Framework;
using ReChordNize.Backend.Model.Models;
using Xunit;

namespace ReChordNizeTest
{
    public class SkillTypesForDailyGoal
    {
        [Test]
        public void ValueRangeForDailySkill()
        {
            foreach (var i1 in SkillTypeUtil.GetSkillTypeForDailyGoal)
            {
                var i = (SkillType)i1;
                SkillTypeUtil.GetValueRangeForDailySkill(i); //No exceptions
                SkillTypeUtil.GetDescription(i);
            }
        }
    }
}