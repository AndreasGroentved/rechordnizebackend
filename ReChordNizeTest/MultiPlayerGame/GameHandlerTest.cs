﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.GameLogic;
using Domain.Time;
using Moq;
using ReChordNize.Backend.Database;
using ReChordNize.Backend.Model.Communication;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using Xunit;
using Xunit.Sdk;

namespace ReChordNizeTest.MultiPlayerGame
{
    public class GameHandlerTest
    {
        private class TestTimeProvider : ITimeProvider
        {
            public DateTime Time { get; set; } = DateTime.Now;

            public DateTime Now => Time;
        }

        private readonly TestTimeProvider _timeProvider = new TestTimeProvider();

        private GameHandler GetGameHandler()
        {
            var levelRepo = new Mock<ILevelRepo>();
            levelRepo.Setup(_ => _.GetLevelWithIdentifier(It.IsAny<LevelIdentifierEnum>())).ReturnsAsync(
                (LevelIdentifierEnum identifier) =>
                    LevelDataBaseCreationData.GetNames().First(a => a.Identifier == identifier));

            return new GameHandler(levelRepo.Object, _timeProvider);
        }


        private const string TestPlayerId = "test";

        [Fact]
        public async Task CreateNonRunningGame()
        {
            var gameHandler = GetGameHandler();
            await GetNonRunningGame(gameHandler);
            var nonRunningGames = gameHandler.NonRunningGames();
            Assert.True(nonRunningGames.Count == 1, "Created a new non running game");
            await gameHandler.CreateNonRunningGame(LevelIdentifierEnum.EasyChordNames, Guid.NewGuid().ToString(),
                TestPlayerId);
            Assert.Equal(2, gameHandler.NonRunningGames().Count);
        }


        [Fact]
        public async Task StartGame()
        {
            var gameHandler = GetGameHandler();
            GameCreatedResponse res = await GetNonRunningGame(gameHandler);
            await gameHandler.JoinGame(res.GameId, "p1");
            await gameHandler.StartGame(res.GameId);
            var game = gameHandler.RunningGames().FirstOrDefault(a => a.Id == res.GameId) ??
                       throw new NullException("should not be null");
            Assert.Empty(gameHandler.NonRunningGames()); //No longer any non running games
            Assert.Single(gameHandler.RunningGames());
        }

        [Fact]
        public async Task Guess()
        {
            var gameHandler = GetGameHandler();
            GameCreatedResponse res = await GetNonRunningGame(gameHandler);
            await gameHandler.JoinGame(res.GameId, "p1");
            await GoStart(gameHandler, res.GameId);
            var next = await GoNext(gameHandler, 1, res.GameId);
            var guessRes = await GoGuess(gameHandler, next.CorrectList, res.GameId);

            Assert.True(guessRes.Correct, "guess was correct");
        }

        [Fact]
        public async Task Next()
        {
            var gameHandler = GetGameHandler();
            GameCreatedResponse res = await GetNonRunningGame(gameHandler);
            await gameHandler.JoinGame(res.GameId, "p1");

            await Assert.ThrowsAsync<NullException>(async () => await GoNext(gameHandler, 1, res.GameId));

            await gameHandler.StartGame(res.GameId);
            var next = await GoNext(gameHandler, 1, res.GameId);

            Assert.Equal(1, next.RoundNumber);
            Assert.NotEmpty(next.CorrectList);
        }


        private async Task<GuessResponse> GoGuess(GameHandler gameHandler, List<string> guess, string gameId) 
            => await gameHandler.Guess(new GuessInfo(guess, "p1", 1, 1_000), gameId) as GuessResponse 
               ?? throw new NullException("should not be null");

        private async Task<GameStartedResponse> GoStart(GameHandler gameHandler, string gameId) =>
            await gameHandler.StartGame(gameId) as GameStartedResponse 
            ?? throw new NullException("should not be null");

        private async Task<NextResponse> GoNext(GameHandler gameHandler, int round, string id) =>
            await gameHandler.Next(round, id) as NextResponse 
            ?? throw new NullException("should not be null");

        private async Task<GameEndResponse> GoEndGame(GameHandler gameHandler, string gameId) =>
            await gameHandler.EndGame(gameId) as GameEndResponse 
            ?? throw new NullException("should not be null");

        [Fact]
        public async Task EndGame()
        {
            var gameHandler = GetGameHandler();
            GameCreatedResponse res = await GetNonRunningGame(gameHandler);
            await gameHandler.JoinGame(res.GameId, "p1");
            await gameHandler.StartGame(res.GameId);
            var endGameRes = await GoEndGame(gameHandler, res.GameId);
            const int expectedPlayerCount = 2;
            Assert.Equal(expectedPlayerCount, endGameRes.UserIdToScoreDictionary.Count);
            Assert.True(endGameRes.UserIdToScoreDictionary.All(a => a.Value == 0),
                "no correct guesses added => score = 0 for all");
        }

        private static async Task<GameCreatedResponse> GetNonRunningGame(GameHandler gameHandler)
            => await gameHandler.CreateNonRunningGame(LevelIdentifierEnum.EasyChordNames,
                   Guid.NewGuid().ToString(), TestPlayerId) as GameCreatedResponse 
               ?? throw new NullException("should not be null");
    }
}