﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Domain.GameLogic;
using ReChordNize.Backend.Model.Models.MultiPlayerGame;
using Xunit;
using Xunit.Sdk;

namespace ReChordNizeTest.MultiPlayerGame
{
    public class GameTest
    {
        private readonly DateTime _time = DateTime.Now;
        private const string DefaultPlayer = "p1";
        private const string Player2 = "p2";
        private const string Player3 = "p3";
        private const string Player4 = "p4";
        private const int DefaultGuessTime = 1_000;
        private readonly List<string> _testPlayables = new List<string> {"A min", "C min", "E min"};

        private Game GetSimpleGameWithDefaultPlayer(DateTime time)
        {
            var state = new GameInfo(_testPlayables, "p1", time, LevelIdentifierEnum.EasyChordNames);
            var game = new Game(state, "test");
            game.JoinGame(DefaultPlayer);
            return game;
        }


        private Game GetAndStartGame()
        {
            var game = GetSimpleGameWithDefaultPlayer(_time);
            game.Start(_time);
            Assert.True(game.HasStarted(), "game has started");
            return game;
        }

        private void AssertNoRoundsAdded(Game game)
        {
            Assert.True(game.GameInfo.Rounds.IsNullOrEmpty(), "No rounds created");
        }

        private async Task AddFirstRound(Game game)
        {
            var next = await game.GetNext(1) ?? throw new NullException("should not be null");
            Assert.True(game.GameInfo.Rounds.Count == 1, "added a round");
        }

        private async Task AddOutOfRangeRound(Game game)
        {
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await game.GetNext(3));
        }

        private async Task AddingRoundTwo(Game game)
        {
            const int roundTwo = 2;
            await game.GetNext(roundTwo);
            Assert.True(game.GameInfo.Rounds.Count == roundTwo, "added a round");
            Assert.True(game.GameInfo.Rounds.Last().PlayableNames.Count == 1,
                "returned a length of 1 for returned playables");
        }

        private Game GetAndStartGameWithThreePlayers()
        {
            var game = GetAndStartGame();
            game.JoinGame(Player2);
            game.JoinGame(Player3);
            return game;
        }


        private async Task<Dictionary<string, int>> AddTwoRoundsGiveP1TwoPointsAndP2OnePoint(Game game)
        {
            var first = await game.GetNext(1) ?? throw new NullException("should not be null");
            var second = await game.GetNext(2) ?? throw new NullException("should not be null");

            game.Guess(new GuessInfo(first, DefaultPlayer, 1, DefaultGuessTime));
            game.Guess(new GuessInfo(second, DefaultPlayer, 2, DefaultGuessTime));
            game.Guess(new GuessInfo(first, Player2, 1, DefaultGuessTime));

            return game.End();
        }

        private void CheckThatGameTimerCanCancel(Game game)
        {
            game.CancelStartGameTimer();
            Assert.False(game.IsStarting());
        }

        private void CheckThatGameChangeStateToIsStarting(Game game)
        {
            game.StartGameSubject.OnNext(false);
            const int unnecessaryLongTime = 100_000;
            game.StartStartGameTimer(unnecessaryLongTime);
            Assert.True(game.IsStarting());
        }

        private async Task CheckGameSubjectStarts(Game game)
        {
            game.StartStartGameTimer(1);
            var didGameStart = await game.StartGameSubject.FirstAsync();
            Assert.True(didGameStart);
        }

        private void AddPlayerToEmptyGameAndAssertHostIsUpdated(Game game)
        {
            var joined = game.JoinGame(Player3);
            Assert.True(joined);
            Assert.Equal(Player3, game.GameInfo.HostPlayer);
        }

        private void RemoveAllPlayersAndAssertThatThereIsNoHost(Game game)
        {
            var removedP2 = game.RemovePlayer(Player2);
            Assert.True(removedP2);
            Assert.Equal("", game.GameInfo.HostPlayer);
        }

        private void AddPlayerAndRemoveHostAndAssertNewHost(Game game)
        {
            game.JoinGame(Player2);
            var removedP1 = game.RemovePlayer(DefaultPlayer);
            Assert.True(removedP1);
            Assert.Equal(Player2, game.GameInfo.HostPlayer);
        }

        [Fact]
        public async void CheckCorrectGuessIsCorrect()
        {
            var game = GetAndStartGame();
            var next = await game.GetNext(1) ?? throw new NullException("should not be null");
            var correct = game.Guess(new GuessInfo(next, DefaultPlayer, 1, DefaultGuessTime));
            Assert.True(correct, "correct guess is correct");
        }

        [Fact]
        public async void CheckInCorrectGuessIsInCorrect()
        {
            var game = GetAndStartGame();
            await game.GetNext(1);
            await game.GetNext(2);
            var correct =
                game.Guess(new GuessInfo(new List<string> {"not correct"}, DefaultPlayer, 2, DefaultGuessTime));
            Assert.False(correct, "incorrect guess is incorrect");
        }


        [Fact]
        public async void CheckInvalidPlayerThrowsException()
        {
            var game = GetAndStartGame();
            await game.GetNext(1);
            const string invalidPlayer = "invalid";
            Assert.Throws<InvalidOperationException>(() =>
                game.Guess(new GuessInfo(new List<string> {"not important"}, invalidPlayer, 1, DefaultGuessTime)));
        }

        [Fact]
        public async Task End()
        {
            var game = GetAndStartGameWithThreePlayers();
            var endResult = await AddTwoRoundsGiveP1TwoPointsAndP2OnePoint(game);

            Assert.Equal(2, endResult[DefaultPlayer]);
            Assert.Equal(1, endResult[Player2]);
            Assert.Equal(0, endResult[Player3]);
        }

        [Fact]
        public async Task GameTimer()
        {
            var time = DateTime.Now;
            var game = GetSimpleGameWithDefaultPlayer(time);

            await CheckGameSubjectStarts(game);
            CheckThatGameChangeStateToIsStarting(game);
            CheckThatGameTimerCanCancel(game);
        }

        [Fact]
        public void JoinGame()
        {
            var game = GetSimpleGameWithDefaultPlayer(_time);
            var joined = game.JoinGame(Player2);
            Assert.True(joined);
            Assert.Equal(DefaultPlayer, game.GameInfo.HostPlayer);
            Assert.True(game.GameInfo.PlayerMap.ContainsKey(DefaultPlayer));
            Assert.True(game.GameInfo.PlayerMap.ContainsKey(Player2));
        }

        [Fact]
        public void JoinGameWithNewHost()
        {
            var game = GetSimpleGameWithDefaultPlayer(_time);

            AddPlayerAndRemoveHostAndAssertNewHost(game);
            RemoveAllPlayersAndAssertThatThereIsNoHost(game);
            AddPlayerToEmptyGameAndAssertHostIsUpdated(game);
        }

        [Fact]
        public async Task Next()
        {
            var game = GetAndStartGame();

            AssertNoRoundsAdded(game); //Now 0 rounds
            await AddFirstRound(game); //Round 1 - now 1 round
            await AddOutOfRangeRound(game); //Add round 3 should fail - still 1 round
            await AddingRoundTwo(game); //Add round 2 -> now two rounds
        }

        [Fact]
        public void Start()
        {
            var game = GetAndStartGame();
            Assert.Equal(_time, game.GameInfo.StartTime);
        }
    }
}